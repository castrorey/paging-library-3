package com.castrorr.paginglibraryv3

import androidx.paging.rxjava2.RxPagingSource
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class UserDataSource(private val repository: Repository): RxPagingSource<Int, User>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, User>> {
        val page = params.key ?: 1
        return repository.getUsers(page)
            .subscribeOn(Schedulers.io())
            .map {
                LoadResult.Page(
                    it.list,
                    null,
                    it.nextPage + 1
                )
            }
    }
}