package com.castrorr.paginglibraryv3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView

class UserAdapter
    : PagingDataAdapter<User, UserAdapter.ViewHolder>(diffCallback = callback) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        return ViewHolder(root)
    }

    class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView) {
        private val textView: TextView = containerView.findViewById(android.R.id.text1)
        fun bind(user: User?) {
            textView.text = "${user?.name?.first} ${user?.name?.last}"
        }
    }

    companion object {
        private  val callback  = object:  ItemCallback<User> () {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

}