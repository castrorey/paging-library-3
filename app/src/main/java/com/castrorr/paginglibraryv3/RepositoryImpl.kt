package com.castrorr.paginglibraryv3

import android.util.Log
import io.reactivex.Single

class RepositoryImpl(private val service: UserService): Repository {

    override fun getUsers(page: Int): Single<Paging<User>> {
        return service.getUsers(page, 15, "abc").map {
            Paging(it.results, it.info.page)
        }.doOnError { Log.e( "getUsers", "${it.message}") }
    }
}