package com.castrorr.paginglibraryv3

data class Paging<T>(
    val list: List<T>,
    var nextPage: Int = 1
)