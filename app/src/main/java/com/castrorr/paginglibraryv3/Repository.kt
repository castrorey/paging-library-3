package com.castrorr.paginglibraryv3

import io.reactivex.Single

interface Repository {
    fun getUsers(page: Int): Single<Paging<User>>
}