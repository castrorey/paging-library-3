package com.castrorr.paginglibraryv3

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava2.flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class ViewModel(private val repository: Repository) {

    private val disposables = CompositeDisposable()

    val state by lazy { PublishSubject.create<UserState>() }

    private val pager by lazy {
        Pager(
            config = PagingConfig(pageSize = 15, enablePlaceholders = false),
            pagingSourceFactory = { UserDataSource(repository) }
        ).flowable
    }

    fun getUsers() {
        pager
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { state.onNext(UserState.Success(it)) },
                onError = { state.onNext(UserState.Error(it)) }
            ).addTo(disposables)
    }
}