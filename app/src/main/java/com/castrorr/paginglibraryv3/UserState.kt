package com.castrorr.paginglibraryv3

import androidx.paging.PagingData

sealed class UserState {
    data class Success(val items: PagingData<User>): UserState()
    data class Error(val error: Throwable) : UserState()
}