package com.castrorr.paginglibraryv3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.paging.map
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ViewModel
    private val adapter by lazy { UserAdapter() }

    private fun handleUserState(state: UserState) {
        when (state) {
            is UserState.Success -> adapter.submitData(lifecycle, state.items)
            is UserState.Error -> Toast.makeText(this, "${state.error.message}", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViewModel()
        setUpViews()
        viewModel.state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { handleUserState(it) },
                onError = { Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show() }
            ).addTo(compositeDisposable = CompositeDisposable())

        viewModel.getUsers()
    }

    private fun setUpViewModel() {
        val service = (application as App).service()
        viewModel = ViewModel(RepositoryImpl(service))
    }

    private fun setUpViews() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
    }
}