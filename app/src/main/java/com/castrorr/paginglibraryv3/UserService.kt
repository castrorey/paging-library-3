package com.castrorr.paginglibraryv3

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UserService {

    // https://randomuser.me/api/?page=3&results=15&seed=abc
    @GET("/api")
    fun getUsers(
        @Query("page") page: Int,
        @Query("results") results: Int,
        @Query("seed") seed: String,
    ): Single<UsersResponse>
}